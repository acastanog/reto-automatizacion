package com.choucair.reto.definition;

import com.choucair.reto.steps.LoginSerenitySteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginSerenityDefinition {

	@Steps
	LoginSerenitySteps loginserenitysteps;
	
	@Given("^accedo al formulario del login$")
	public void accedo_al_formulario_del_login() {
	    loginserenitysteps.open();
	}

	@When("^diligencio el formluario del login con usuario \"([^\"]*)\" y pass \"([^\"]*)\"$")
	public void diligencio_el_formluario_del_login_con_usuario_y_pass(String username, String password) {
	    loginserenitysteps.ingresar_datos(username, password);
	}

	@Then("^se inicia sesion correctamente$")
	public void se_inicia_sesion_correctamente() {
	   loginserenitysteps.valido_inicio_sesion();
	}


}
